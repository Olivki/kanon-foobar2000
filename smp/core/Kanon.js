"use strict"

include(`${fb.FoobarPath}skins\\Kanon\\SMP\\core\\utils\\Logger.js`);

// This is just for a nicer syntax for all the scripts that include this one.
class Kanon {
    
    static get SKIN_PATH() { return `${fb.FoobarPath}skins\\Kanon\\` }
    
    static get SETTINGS_PATH() { return `${this.SKIN_PATH}settings\\` }
    
    static get ASSETS_PATH() { return `${this.SKIN_PATH}assets\\` }
    
    static get CORE_PATH() { return `${this.SKIN_PATH}SMP\\core\\` }
    
    static get LEGACY_PATH() { return `${this.SKIN_PATH}SMP\\legacy\\jscript\\` }
    
    static get PANELS_PATH() { return `${this.SKIN_PATH}SMP\\panels\\` }
    
    static get VERSION() { return "2.0.0-alpha" }
    
}

// Note to self:
// If file A has an instance in file B, and both need to include file C, file B should be the one to include it.
// Otherwise things will hit the fan.
//const kanon = new Kanon();
const log = new Logger("Kanon");

/**
 * Includes any file inside the docs directory for the Spider Monkey Panel component that match the given name.
 *
 * @param {!string} name - The name of the JS file, excluding the ".js".
 * @author Olivki
 */
function import_default(name) {
    include(`${fb.ComponentPath}docs\\${name.replace(".", "\\")}.js`);
}

/**
 * Includes any file inside the core directory inside the Kanon skin directory.
 * Also includes any files inside any sub-folders if given the correct syntax.
 *
 * @example
 * import_element("files.Reader");
 * @example
 * import_element("files\\Reader");
 * @param {!string} name - The name of the JS file, excluding the ".js".
 * @author Olivki
 */
function import_core(name) {
    include(`${Kanon.CORE_PATH}${name.replace(".", "\\")}.js`)
}

/**
 * Includes any file inside the util directory inside the core directory.
 * Also includes any files inside any sub-folders if given the correct syntax.
 *
 * @example
 * import_util("Logger");
 * @example
 * import_util("files.Reader");
 * @param {!string} name - The name of the JS file, excluding the ".js".
 * @author Olivki
 */
function import_util(name) {
    import_core(`utils\\${name}`);
}

/**
 * Includes the "lodash.js" file inside of the ./utils/ folder.
 *
 * @example
 * import_lodash();
 * @author Olivki
 */
function import_lodash() {
    import_core(`utils\\lodash.js`)
}

/**
 * Includes any file inside the service directory inside the core directory.
 * Also includes any files inside any sub-folders if given the correct syntax.
 *
 * @example
 * import_service("LastFM");
 * @example
 * import_service("Steven");
 * @param {!string} name - The name of the JS file, excluding the ".js".
 * @author Olivki
 */
function import_service(name) {
    import_core(`services\\${name}`)
}

/**
 * Includes any file inside the element directory inside the core directory.
 * Also includes any files inside any sub-folders if given the correct syntax.
 *
 * @example
 * import_element("VolumeSlider");
 * @example
 * import_element("base.Slider");
 * @param {!string} name - The name of the JS file, excluding the ".js".
 * @author Olivki
 */
function import_element(name) {
    import_core(`elements\\${name}`);
}

/**
 * Includes any file inside the core directory inside the Kanon skin directory.
 * Also includes any files inside any sub-folders if given the correct syntax.
 *
 * @example
 * import_legacy("DoomsdayMachine");
 * @example
 * import_legacy("Essential.VariableHelpers");
 * @param {!string} name - The name of the JS file, excluding the ".js".
 * @author Olivki
 */
function import_legacy(name) {
    include(`${Kanon.LEGACY_PATH}${name.replace(".", "\\")}.js`)
}

/**
 * Includes any file inside the core directory inside the samples directory inside the component folder for the
 * Spider Monkey Panel component. These are essentially all made by marc2003.
 * Also includes any files inside any sub-folders if given the correct syntax.
 *
 * @example
 * import_sample("DoomsdayMachine");
 * @example
 * import_sample("Essential.VariableHelpers");
 * @param {!string} name - The name of the JS file, excluding the ".js".
 * @author Olivki
 */
function import_sample(name) {
    include(`${fb.ComponentPath}samples\\${name.replace(".", "\\")}.js`);
}

String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) > 0;
};

String.prototype.contains = function (it) {
    return this.indexOf(it) > 0;
};