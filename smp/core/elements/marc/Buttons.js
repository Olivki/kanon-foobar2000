// Original versions of these classes were made by marc2003, so credit goes to him.

class Button {
    constructor(x, y, width, height, image, fn, toolTipText) {
        this.x = x
        this.y = y
        this.w = width
        this.h = height
        this.fn = fn
        this.tiptext = toolTipText
        this.img_normal = typeof image.normal == "string" ? _img(image.normal) : image.normal
        this.img_hover =
            image.hover ? (typeof image.hover == "string" ? _img(image.hover) : image.hover) : this.img_normal
        this.img = this.img_normal
    }
}

function _button(x, y, w, h, img_src, fn, tiptext) {
    this.paint = (gr) => {
        if (this.img) {
            _drawImage(gr, this.img, this.x, this.y, this.w, this.h)
        }
    }
    
    this.trace = (x, y) => {
        return x > this.x && x < this.x + this.w && y > this.y && y < this.y + this.h
    }
    
    this.lbtn_up = (x, y, mask) => {
        if (this.fn) {
            this.fn(x, y, mask)
        }
    }
    
    this.cs = (s) => {
        if (s == "hover") {
            this.img = this.img_hover
            _tt(this.tiptext)
        } else {
            this.img = this.img_normal
        }
        window.RepaintRect(this.x, this.y, this.w, this.h)
    }
    
    
}

class Buttons {
    
    constructor() {
        this.buttons = {}
        this.button = null
    }
    
    paint(graphics) {
        _.invokeMap(this.buttons, "paint", graphics)
    }
    
    move(x, y) {
        let tempButton = null
        
        _.forEach(this.buttons, (item, i) => {
            if (item.trace(x, y)) {
                tempButton = i
            }
        })
        
        if (this.button === tempButton) return this.button
        
        if (this.button) this.buttons[this.button].cs("normal")
        
        if (tempButton) this.buttons[tempButton].cs("hover") else _tt("")
        
        this.button = tempButton
        
        return this.button
    }
    
    leave() {
        if (this.btn) {
            _tt("")
            this.buttons[this.btn].cs("normal")
        }
        this.btn = null
    }
    
    lbtn_up(x, y, mask) {
        if (this.button) {
            this.buttons[this.button].lbtn_up(x, y, mask)
            return true
        }
        
        return false
    }
}