include(`${fb.FoobarPath}skins\\Kanon\\SMP\\core\\Kanon.js`)
window.DefinePanel("Track Lover", { author: "Olivki" })

import_default("Helpers")
import_util("ui.Graphics")
import_legacy("Essential.VariableHelpers")
import_legacy("Essential.ColorHelper")

let isHovering = false
let isHoveringOverHeart = false
const font = gdi.Font("Segoe UI", 30, 1)
let colour = BasicColors.White
let handle = fb.GetNowPlaying()
let rating = TitleFormat("$if2(%smp_rating%,0)", handle)

const Positions = {
    X: 0,
    Y: 0,
    Width: 0,
    Height: 0
}

function updatePositions() {
    Positions.X = 0
    Positions.Y = 0
    Positions.Width = window.Width
    Positions.Height = window.Height
}

function on_size() {
    updatePositions()
}

function on_paint(graphics) {
    onUpdate()
    graphics.SetTextRenderingHint(TextRenderingHint.AntiAlias)
    
    if (isHovering) {
        DrawBorderedRect(0, 0, Positions.Width, Positions.Height, HoverInterfaceColors.Background,
                         InterfaceColors.DarkLine, graphics)
        graphics.DrawString("♥", font, colour, 0, 0, Positions.Width, Positions.Height - 2,
                            StringFormat(StringAlignment.Center, StringAlignment.Center))
    }
}

function onUpdate() {
    handle = fb.GetNowPlaying()
    rating = TitleFormat("$if2(%rating%,0)", handle)
    
    if (isHoveringOverHeart) {
        if (rating > 3) {
            colour = HoverInterfaceColors.ActiveHeartHighlight
        } else {
            colour = HoverInterfaceColors.InactiveHeartHighlight
        }
    } else if (rating > 3) {
        colour = HoverInterfaceColors.ActiveHeart
    } else {
        colour = HoverInterfaceColors.InactiveHeart
    }
}

function on_mouse_lbtn_down(x, y) {
    if (isHoveringOverHeart) {
        setTrackRating(rating > 3 ? 0 : 5, handle)
        window.Repaint()
    }
}

function on_mouse_rbtn_down(x, y) {
    if (isHoveringOverHeart) {
        const heartMenu = window.CreatePopupMenu()
        const trackRatingMenu = window.CreatePopupMenu()
        
        trackRatingMenu.AppendTo(heartMenu, MF_STRING, "Track Rating")
        heartMenu.AppendMenuItem(MF_SEPARATOR, 0, 0)
        heartMenu.AppendMenuItem(MF_STRING, 1, "Configure")
        
        // - Rating Buttons -
        trackRatingMenu.AppendMenuItem(MF_STRING, 2, "1")
        trackRatingMenu.AppendMenuItem(MF_STRING, 3, "2")
        trackRatingMenu.AppendMenuItem(MF_STRING, 4, "3")
        trackRatingMenu.AppendMenuItem(MF_STRING, 5, "4")
        trackRatingMenu.AppendMenuItem(MF_STRING, 6, "5")
        
        const heartMenuAction = heartMenu.TrackPopupMenu(x, y)
        
        switch (heartMenuAction) {
            case 1:
                window.ShowConfigure()
                //fb.ShowConsole();
                break
        }
        
        if (heartMenuAction > 1) {
            setTrackRating(heartMenuAction - 1, handle)
        }
        
        trackRatingMenu.Dispose()
        heartMenu.Dispose()
    }
}

function on_mouse_move(x, y) {
    isHovering = true
    
    isHoveringOverHeart =
        isWithinBounds(x, y, Positions.X + 21, Positions.Y + 7, Positions.Width - 42, Positions.Height - 12)
    
    window.SetCursor(isHoveringOverHeart ? IDC_HAND : IDC_ARROW)
    
    window.Repaint()
}

function on_mouse_leave() {
    isHovering = false
    isHoveringOverHeart = false
    window.SetCursor(IDC_ARROW)
    window.Repaint()
}

function on_playback_time(time) {
    window.Repaint()
}