"use strict"

include(`${fb.FoobarPath}skins\\Kanon\\SMP\\core\\Kanon.js`)
window.DefinePanel("last.fm control", { author: "Olivki" })

import_service("LastFM")
import_util("Variables")
import_util("Common")
import_util("MD5")
include(`${fb.FoobarPath}skins\\Kanon\\smp\\marc\\js\\lodash.min.js`)
include(`${fb.FoobarPath}skins\\Kanon\\smp\\marc\\js\\helpers.js`)
include(`${fb.FoobarPath}skins\\Kanon\\smp\\marc\\js\\panel.js`)

const imagePath = `${Kanon.ASSETS_PATH}buttons\\last.fm\\`

const panel = new _panel(true)
const buttons = new _buttons()
const lastfm = new LastFM()

log.debug(`${imagePath}error.png`)

buttons.update = () => {
    let n, h, func, tooltip
    switch (true) {
        case lastfm.username.length === 0 || lastfm.sessionKey.length !== 32:
            n = `${imagePath}error.png`
            h = `${imagePath}error.png`
            func = null
            tooltip = "Right click to set your Last.fm username and authorise."
            break
        case !panel.metadb:
            n = `${imagePath}error.png`
            h = `${imagePath}error.png`
            func = null
            tooltip = "No selection"
            break
        /*case parseInt(panel.tf("%SMP_LOVED%")) === 1:
         n = loved
         h = unloved
         func = () => {
         lastfm.post("track.unlove", null, panel.metadb)
         }
         tooltip = panel.tf("Unlove %title% by %artist%")
         break*/
        default:
            n = `${imagePath}logo.png`
            h = `${imagePath}logo.png`
            func = () => {
                lastfm.post("track.love", null, panel.metadb)
            }
            tooltip = panel.tf("Love %title% by %artist%")
            break
    }
    buttons.buttons.lover = new _button(0, 0, 32, 32, {
        normal: n,
        hover: h
    }, func, tooltip)
}

function on_colours_changed() {
    panel.colours_changed()
    window.Repaint()
}

function on_item_focus_change() {
    panel.item_focus_change()
}

function on_metadb_changed() {
    buttons.update()
    window.Repaint()
}

function on_mouse_leave() {
    buttons.leave()
}

function on_mouse_lbtn_up(x, y) {
    buttons.lbtn_up(x, y)
}

function on_mouse_move(x, y) {
    buttons.move(x, y)
}

function on_mouse_rbtn_up(x, y) {
    if (buttons.buttons.lover.trace(x, y)) {
        let menu = window.CreatePopupMenu()
        menu.AppendMenuItem(MF_STRING, 1, "Set and authorise your Last.fm username")
        menu.AppendMenuItem(lastfm.sessionKey.length === 32 ? MF_STRING : MF_GRAYED, 2,
                            "Bulk import Last.fm loved tracks")
        menu.AppendMenuSeparator()
        menu.AppendMenuItem(MF_STRING, 3, "Show loved tracks")
        menu.AppendMenuSeparator()
        menu.AppendMenuItem(MF_STRING, 4, "Configure...")
        const id = menu.TrackPopupMenu(x, y)
        switch (id) {
            case 1:
                if (lastfm.showAuthWindow() && lastfm.username.length) {
                    lastfm.post("auth.getToken")
                }
                buttons.update()
                window.Repaint()
                break
            case 2:
                lastfm.getLovedTracks(1)
                break
            case 3:
                fb.ShowLibrarySearchUI("%SMP_LOVED% IS 1")
                break
            case 4:
                window.ShowConfigure()
                break
        }
    } else {
        panel.rbtn_up(x, y)
    }
    return true
}

function on_notify_data(name, data) {
    lastfm.onNotifyData(name, data)
}

function on_paint(gr) {
    panel.paint(gr)
    buttons.paint(gr)
}

function on_playback_dynamic_info_track() {
    panel.item_focus_change()
}

function on_playback_new_track() {
    panel.item_focus_change()
    lastfm.onPlaybackNewTrack()
}

function on_playback_stop(reason) {
    if (reason !== 2) {
        panel.item_focus_change()
    }
}

function on_playback_time() {
    lastfm.onPlayBackTime()
}

function on_playlist_switch() {
    panel.item_focus_change()
}

function on_size() {
    panel.size()
    buttons.update()
}
