"use strict"

// The original of this class was made by marc2003, so most of the credit goes to him.
class LastFM {
    
    constructor() {
        this.formattedTitles = {
            key: fb.TitleFormat("$lower(%artist% - %title%)"),
            artist: fb.TitleFormat("%artist%"),
            title: fb.TitleFormat("%title%"),
            album: fb.TitleFormat("[%album%]")
        }
    
        this.storageFolder = Files.createDirectories(`${Kanon.SETTINGS_PATH}last.fm`)
        this.variableFile = `${this.storageFolder}\\variables.ini`
        this.apiKey = "332912b4e81060cfae1f1f500ca50663"
        this.secret = "c102440ec94ea42fdd11009e457f1698"
        this.username = this.getVariable("username")
        this.sessionKey = this.getVariable("session_key")
        this.userAgent = "kanon-lastfm"
        this.xmlhttp = new ActiveXObject("Microsoft.XMLHTTP")
    
        this.timeElapsed = 0
        this.minimumLength = 30
        this.globalHandle = fb.GetNowPlaying()
        
        this.logger = new CustomLogger("last.fm")
    
        this.getActions = new Map()
        this.getActions.set("track.getInfo", new GetTrackInfoGetAction(this))
    
        this.postActions = new Map()
        
        // Auth Actions
        this.postActions.set("auth.getToken", new GetTokenPostAction(this))
        this.postActions.set("auth.getSession", new GetSessionKeyPostAction(this))
        
        // Track Actions
        this.postActions.set("track.love", new LoveTrackPostAction(this))
        this.postActions.set("track.unlove", new UnLoveTrackPostAction(this))
        this.postActions.set("track.scrobble", new ScrobblePostAction(this))
        this.postActions.set("track.updateNowPlaying", new NowPlayingPostAction(this))
    }
    
    onNotifyData(name, data) {
        if (name === "2K3.NOTIFY.LASTFM") {
            this.username = this.getVariable("username")
            this.sessionKey = this.getVariable("session_key")
            if (typeof buttons == "object" && typeof buttons.update == "function") {
                buttons.update()
                window.Repaint()
            }
            _.forEach(panel.list_objects, (item) => {
                if (item.mode === "lastfm_info" && item.properties.mode.value > 0) {
                    item.update()
                }
            })
        }
    }
    
    reCheckVariables() {
        if (this.globalHandle == null) this.globalHandle = fb.GetNowPlaying()
        if (this.timeElapsed == null) this.timeElapsed = 0
        if (this.timestamp == null) this.timestamp = _.floor(_.now() / 1000)
        if (this.targetTime == null) Math.min(_.ceil(fb.PlaybackLength / 2), 240)
    }
    
    onPlaybackNewTrack() {
        if (!this.shouldExecute()) return
        
        this.globalHandle = fb.GetNowPlaying()
        this.timeElapsed = 0
        this.timestamp = _.floor(_.now() / 1000)
        this.targetTime = Math.min(_.ceil(fb.PlaybackLength / 2), 240)
        
        if (this.globalHandle != null && fb.IsMetadbInMediaLibrary(this.globalHandle)) {
            this.logger.info("Playing new track..")
            this.logger.info(`Will scrobble to last.fm at ${utils.FormatDuration(this.targetTime)}.`)
            this.post("track.updateNowPlaying", null, this.globalHandle)
        }
    }
    
    onPlayBackTime() {
        if (this.globalHandle == null || !this.shouldExecute()) return
        
        this.timeElapsed = Math.round(fb.PlaybackTime) // TODO: Change back to inceasing it.
        this.reCheckVariables()
        
        if (this.timeElapsed === this.targetTime) {
            if (!fb.IsMetadbInMediaLibrary(this.globalHandle)) {
                console.log("Skipping... Track not in Media Library.")
            } else if (fb.PlaybackLength < this.minimumLength) {
                console.log("Not submitting. Track too short.")
                // still check to see if a track is loved even if it is too short to scrobble
                this.getTrackInfo(this.globalHandle)
            } else {
                this.attempt = 1
                this.post("track.scrobble", null, this.globalHandle)
            }
        }
    }
    
    getSanitizedArtist(handle) {
        return Util.clawJapaneseNameFromEnclosure(this.formattedTitles.artist.Eval(true))
    }
    
    post(method, token, handle) {
        let api_sig, data
    
        if (!this.postActions.has(method)) return this.logger.error(`Unknown method: "${method}".`)
        
        switch (method) {
            case "auth.getToken":
            case "auth.getSession":
                this.postActions.get(method).performPost(token, handle)
                break
    
            default:
                if (!handle) return this.logger.error("Can't do actions when the handle is null.")
                this.postActions.get(method).performPost(token, handle)
                break
        }
    }
    
    getLovedTracks(p) {
        if (!this.username.length) {
            return console.log(N, "Last.fm Username not set.")
        }
        this.page = p
        const url = this.getBaseUrl() + "&method=user.getLovedTracks&limit=200&user=" + this.username + "&page=" +
                    this.page
        this.xmlhttp.open("GET", url, true)
        this.xmlhttp.setRequestHeader("User-Agent", this.userAgent)
        this.xmlhttp.setRequestHeader("If-Modified-Since", "Thu, 01 Jan 1970 00:00:00 GMT")
        this.xmlhttp.send()
        this.xmlhttp.onreadystatechange = () => {
            if (this.xmlhttp.readyState === 4) {
                this.done("user.getLovedTracks")
            }
        }
    }
    
    getTrackInfo(handle) {
        this.getActions.get("track.getInfo").performGet(handle, null)
    }
    
    done(method, meta_db) {
        const artist = this.getSanitizedArtist(meta_db)
        const track = this.formattedTitles.title.EvalWithMetadb(meta_db)
        
        let data
        switch (method) {
            case "user.getLovedTracks":
                data = _jsonParse(this.xmlhttp.responseText)
    
                if (this.page === 1) {
                    fb.ShowConsole()
                    if (data.error) {
                        return console.log(N, "Last.fm server error:", data.message)
                    }
                    this.loved_tracks = []
                    this.pages = _.get(data, "lovedtracks[\"@attr\"].totalPages", 0)
                }
    
                data = _.get(data, "lovedtracks.track", [])
    
                if (data.length) {
                    this.loved_tracks = [...this.loved_tracks, ...(_.map(data, (item) => {
                        const artist = item.artist.name.toLowerCase()
                        const title = item.name.toLowerCase()
                        return artist + " - " + title
                    }))]
        
                    this.logger.info(`Loved tracks: completed page ${this.page}/${this.pages}`)
                }
                if (this.page < this.pages) {
                    this.page++
                    this.getLovedTracks(this.page)
                } else {
                    this.logger.info(`${this.loved_tracks.length} loved tracks were found on last.fm.`)
                    
                    let items = fb.GetLibraryItems()
                    items.OrderByFormat(this.formattedTitles.key, 1)
                    let items_to_refresh = new FbMetadbHandleList()
    
                    for (let i = 0; i < items.Count; i++) {
                        let m = items[i]
                        let current = this.formattedTitles.key.EvalWithMetadb(m)
                        let idx = _.indexOf(this.loved_tracks, current)
                        if (idx > -1) {
                            this.loved_tracks.splice(idx, 1)
                            m.SetLoved(1)
                            items_to_refresh.Add(m)
                        }
                    }
    
                    this.logger.info(
                        `${items_to_refresh.Count} library tracks matched and updated. Duplicates are not counted`)
    
                    this.logger.info(
                        "For those updated tracks, %SMP_LOVED% now has the value of 1 in all components/search dialogs.")
    
                    if (this.loved_tracks.length) {
                        this.logger.info("The following tracks were not matched:")
                        
                        _.forEach(this.loved_tracks, (item) => {
                            this.logger.info(item)
                        })
                    }
    
                    items_to_refresh.RefreshStats()
                }
                return
        }
        // display response text/error if we get here, any success returned early
        this.logger.debug(this.xmlhttp.responseText || this.xmlhttp.status)
    }
    
    showAuthWindow() {
        try {
            const username = utils.InputBox(window.ID, "Enter your Last.fm username", window.Name, this.username, true)
            if (username !== this.username) {
                this.setVariable("username", username)
                this.setSessionKey("")
            }
            return true
        } catch (e) {
            this.logger.info("Auth window was closed..")
            return false
        }
    }
    
    getBaseUrl() {
        return "http://ws.audioscrobbler.com/2.0/?format=json&api_key=" + this.apiKey
    }
    
    getVariable(key) {
        return utils.ReadINI(this.variableFile, "Last.fm", key)
    }
    
    setVariable(key, value) {
        utils.WriteINI(this.variableFile, "Last.fm", key, value)
    }
    
    setSessionKey(sessionKey) {
        this.setVariable("session_key", sessionKey)
        window.NotifyOthers("2K3.NOTIFY.LASTFM", "update")
        this.onNotifyData("2K3.NOTIFY.LASTFM", "update")
    }
    
    shouldExecute() {
        return !(!this.username.length || this.sessionKey.length !== 32)
    }
}

// Get Actions
class GetAction {
    
    constructor(method, last_fm) {
        this.method = method
        this.last_fm = last_fm
        this.logger = last_fm.logger
        this.xmlHttp = last_fm.xmlhttp
        this.api_key = last_fm.apiKey
        this.secret = last_fm.secret
    }
    
    performGet(handle, extra) {}
    
    afterGet(handle) {}
    
    setVariables(handle) {
        this.sk = this.last_fm.sessionKey
        this.timestamp = this.last_fm.timestamp
        this.artist = this.last_fm.getSanitizedArtist(handle)
        this.track = this.last_fm.formattedTitles.title.EvalWithMetadb(handle)
        this.album = this.last_fm.formattedTitles.album.EvalWithMetadb(handle)
        this.duration = _.round(handle.Length)
        this.builder = new ParameterBuilder()
    }
    
    getData(params, handle) {
        this.xmlHttp.open("GET", `http://ws.audioscrobbler.com/2.0/?${params}`, true)
        this.xmlHttp.setRequestHeader("User-Agent", this.last_fm.userAgent)
        this.xmlHttp.setRequestHeader("If-Modified-Since", "Thu, 01 Jan 1970 00:00:00 GMT")
        this.xmlHttp.send()
        this.builder.clear()
        this.xmlHttp.onreadystatechange = () => {
            if (this.xmlHttp.readyState === 4) {
                if (this.xmlHttp.status === 200) {
                    this.afterGet(handle)
                } else {
                    this.logger.error(`HTTP error: ${this.xmlHttp.status}`)
                    this.xmlHttp.responseText && this.logger.error(this.xmlHttp.responseText)
                }
            }
        }
    }
    
    canPerformGet(handle) {
        this.setVariables(handle)
        if (!this.last_fm.username.length) {
            this.logger.error("Last.fm username not set.")
            return false
        } else if (this.sk.length !== 32) {
            this.logger.error("This script has not been authorised.")
            return false
        }
        
        return true
    }
}

class GetTrackInfoGetAction extends GetAction {
    
    constructor(last_fm) { super("track.getInfo", last_fm) }
    
    performGet(handle, extra) {
        if (!this.canPerformGet(handle)) return
        
        this.logger.info("Getting track info..")
        
        if (!_tagged(this.artist) || !_tagged(this.track)) return this.logger.error(
            `artist (${this.artist} xor track (${this.track} isn't tagged.`)
        
        this.builder.add("format", "json")
        this.builder.add("api_key", this.api_key)
        this.builder.add("method", this.method)
        this.builder.add("user", this.last_fm.username)
        this.builder.add("artist", this.artist, true)
        this.builder.add("track", this.track, true)
        // must use autocorrect now even when it is disabled on website
        this.builder.add("autocorrect", "1")
        this.builder.add("s", _.now())
        
        this.getData(this.builder.WORKING_URL, handle)
    }
    
    afterGet(handle) {
        let data = _jsonParse(this.xmlHttp.responseText)
        
        if (data.error) return this.logger.error(data.message)
        if (!data.track) return this.logger.error("Unexpected server error.")
    
        // Migrate values from playback statistics to SMP if all SMP values are empty.
        if (SmpStats.isEmpty()) {
            SmpStats.runAll(handle)
        } else {
            SmpStats.updateDates(handle)
        }
        
        const loved = parseInt(data.track.userloved)
        const playCount = parseInt(data.track.userplaycount)
        
        const isLoved = loved === 1 ? "is" : "is not"
        
        this.logger.info(`"${Util.getTrackInfo(handle)}" ${isLoved} loved, and it has a play count of ${playCount}.`)
        
        if (loved === 1) {
            SmpStats.setRating(5, handle)
        } // Do nothing if not loved, for now.
    
        if (fb.PlaybackLength < this.minimumLength) return this.logger.error(
            "Track is shorter than the specified minimum length, aborting.")
    
        const oldPlayCount = SmpStats.PLAY_COUNT
        const newPlayCount = playCount
    
        if (newPlayCount < oldPlayCount) {
            this.logger.info("Play count returned from Last.fm is lower than current value, not updating.")
            SmpStats.setPlayCount(SmpStats.PLAY_COUNT + 1, handle)
        } else if (newPlayCount === oldPlayCount) {
            this.logger.info("No changes to play count found, not updating.")
            SmpStats.setPlayCount(SmpStats.PLAY_COUNT + 1, handle)
        } else {
            SmpStats.setPlayCount(newPlayCount, handle)
        }
    }
}

// Post Actions
class PostAction {
    
    constructor(method, last_fm) {
        this.method = method
        this.last_fm = last_fm
        this.logger = last_fm.logger
        this.xmlHttp = last_fm.xmlhttp
        this.api_key = last_fm.apiKey
        this.secret = last_fm.secret
    }
    
    performPost(token, handle) {}
    
    afterPost(handle) {}
    
    addCommonParameters(api_sig, useJson = true) {
        if (useJson) this.builder.add("format", "json")
        this.builder.add("method", this.method)
        this.builder.add("api_key", this.api_key)
        this.builder.add("api_sig", api_sig)
    }
    
    setVariables(handle, nullHandle = false) {
        this.sk = this.last_fm.sessionKey
        this.timestamp = this.last_fm.timestamp
        
        if (!nullHandle) {
            this.artist = this.last_fm.getSanitizedArtist(handle)
            this.track = this.last_fm.formattedTitles.title.EvalWithMetadb(handle)
            this.album = this.last_fm.formattedTitles.album.EvalWithMetadb(handle)
            this.duration = _.round(handle.Length)
        }
        
        this.builder = new ParameterBuilder()
    }
    
    postData(params, handle) {
        this.xmlHttp.open("POST", "https://ws.audioscrobbler.com/2.0/", true)
        this.xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
        this.xmlHttp.setRequestHeader("User-Agent", this.last_fm.userAgent)
        this.xmlHttp.send(params)
        this.builder.clear()
        this.xmlHttp.onreadystatechange = () => {
            if (this.xmlHttp.readyState === 4) {
                this.afterPost(handle)
            }
        }
    }
    
    canPost(handle, allowNullHandle = false) {
        if (!handle) {
            if (allowNullHandle) {
                this.setVariables(handle, true)
            } else {
                this.logger.error(`Can't set variables for post "${this.method}" method when handle is null.`)
                return false
            }
        } else {
            this.setVariables(handle)
        }
        
        if (!this.last_fm.username.length) {
            this.logger.error("Last.fm username not set.")
            return false
        } else if (this.sk.length !== 32) {
            this.logger.error(`This script has not been authorised. (${this.method})`)
            return false
        }
        
        return true
    }
}

// Auth Actions
class GetTokenPostAction extends PostAction {
    
    constructor(last_fm) { super("auth.getToken", last_fm) }
    
    performPost(token, handle) {
        this.setVariables(handle, true)
        
        // Clearing the old token from the system.
        this.last_fm.setSessionKey("")
        
        const api_sig = md5(`api_key${this.api_key}method${this.method}${this.secret}`)
        
        this.addCommonParameters(api_sig)
        
        this.postData(this.builder.WORKING_URL, handle)
    }
    
    afterPost(handle) {
        let data = _jsonParse(this.xmlHttp.responseText)
        
        if (data.token) {
            this.logger.info("Opening auth.")
            Util.execute(`https://last.fm/api/auth/?api_key=${this.api_key}&token=${data.token}`)
            
            if (WshShell.Popup("If you granted permission successfully, click Yes to continue.", 0, window.Name,
                               popup.question + popup.yes_no) === popup.yes) {
                this.last_fm.post("auth.getSession", data.token, handle)
            }
        }
    }
}

class GetSessionKeyPostAction extends PostAction {
    
    constructor(last_fm) { super("auth.getSession", last_fm) }
    
    performPost(token, handle) {
        this.setVariables(handle, true)
        
        const api_sig = md5(`api_key${this.api_key}method${this.method}token${token}${this.secret}`)
        
        this.addCommonParameters(api_sig)
        this.builder.add("token", token)
        
        this.postData(this.builder.WORKING_URL, handle)
    }
    
    afterPost(handle) {
        let data = _jsonParse(this.xmlHttp.responseText)
        
        if (data.session && data.session.key) {
            this.logger.info("Updating session key..")
            this.last_fm.setSessionKey(data.session.key)
        } else if (data.error) {
            this.logger.error(`Session Key Error: ${data.message}`)
        }
    }
}

// Track Actions
class LoveTrackPostAction extends PostAction {
    
    constructor(last_fm) { super("track.love", last_fm) }
    
    performPost(token, handle) {
        if (!this.canPost(handle)) return
        
        if (!Util.isTagged(this.artist) || !Util.isTagged(this.track)) return this.logger.error(
            "Artist xor track isn't tagged.")
        
        this.logger.info(`Attempting to love "${Util.getTrackInfo(handle)}".`)
        this.logger.info("Contacting last.fm...")
        const api_sig = md5(
            `api_key${this.api_key}artist${this.artist}method${this.method}sk${this.sk}track${this.track}${this.secret}`)
        
        // can't use format=json because Last.fm API is broken for this method
        this.addCommonParameters(api_sig, false)
        this.builder.add("sk", this.sk)
        this.builder.add("artist", this.artist, true)
        this.builder.add("track", this.track, true)
        
        this.postData(this.builder.WORKING_URL, handle)
    }
    
    afterPost(handle) {
        if (this.xmlHttp.responseText.includes("ok")) {
            this.logger.info("Track loved successfully.")
            
            SmpStats.setRating(5, handle)
        }
    }
}

class UnLoveTrackPostAction extends PostAction {
    
    constructor(last_fm) { super("track.unlove", last_fm) }
    
    performPost(token, handle) {
        if (!this.canPost(handle)) return
        
        if (!Util.isTagged(this.artist) || !Util.isTagged(this.track)) return this.logger.error(
            "Artist xor track isn't tagged.")
        
        this.logger.info(`Attempting to un-love "${Util.getTrackInfo(handle)}".`)
        this.logger.info("Contacting last.fm...")
        const api_sig = md5(
            `api_key${this.api_key}artist${this.artist}method${this.method}sk${this.sk}track${this.track}${this.secret}`)
        
        // can't use format=json because Last.fm API is broken for this method
        this.addCommonParameters(api_sig, false)
        this.builder.add("sk", this.sk)
        this.builder.add("artist", this.artist, true)
        this.builder.add("track", this.track, true)
        
        this.postData(this.builder.WORKING_URL, handle)
    }
    
    afterPost(handle) {
        if (this.xmlHttp.responseText.includes("ok")) {
            this.logger.info("Track un-loved successfully.")
            
            // Help, I've fallen into the boolean trap and I can't get up
            SmpStats.setRating(0, handle, true, true, true)
        }
    }
}

class ScrobblePostAction extends PostAction {
    
    constructor(last_fm) { super("track.scrobble", last_fm) }
    
    performPost(token, handle) {
        if (!this.canPost(handle)) return
        
        const api_sig = md5(
            `album${this.album}api_key${this.api_key}artist${this.artist}duration${this.duration}method${this.method}sk${this.sk}timestamp${this.timestamp}track${this.track}${this.secret}`)
    
        this.addCommonParameters(api_sig)
        this.builder.add("sk", this.sk)
        this.builder.add("duration", this.duration)
        this.builder.add("timestamp", this.timestamp)
        this.builder.add("album", this.album, true)
        this.builder.add("artist", this.artist, true) // y
        this.builder.add("track", this.track, true) // y
        
        this.postData(this.builder.WORKING_URL, handle)
    }
    
    afterPost(handle) {
        this.attempts = 0
        let data = _jsonParse(this.xmlHttp.responseText)
        
        if (data.error) {
            this.logger.error(`Scrobble Error: ${data.message}`)
        } else {
            data = _.get(data, "scrobbles[\"@attr\"]", [])
            if (data.ignored === 1) {
                this.logger.error(
                    "Track not scrobbled. The submission server refused it possibly because of incomplete tags" +
                    " or incorrect system time.")
            } else if (data.accepted === 1) {
                this.logger.info("Track scrobbled successfully.")
            } else {
                if (this.attempt === 1) this.logger.warn("Unexpected submission server response.")
                if (this.attempt < 5) {
                    this.attempt++
                    
                    this.logger.info("Retrying...")
                    
                    window.SetTimeout(() => {
                        this.performPost(data.token, handle)
                    }, 1000)
                } else {
                    this.logger.error("Submission failed.")
                }
                return
            }
    
            //this.logger.info("Now fetching playcount...")
            
            window.SetTimeout(() => {
                this.last_fm.getTrackInfo(handle)
            }, 1000)
        }
    }
}

class NowPlayingPostAction extends PostAction {
    
    constructor(last_fm) { super("track.updateNowPlaying", last_fm) }
    
    performPost(token, handle) {
        if (!this.canPost(handle)) return
        
        const api_sig = md5(
            `api_key${this.api_key}artist${this.artist}duration${this.duration}method${this.method}sk${this.sk}track${this.track}${this.secret}`)
    
        this.addCommonParameters(api_sig)
        this.builder.add("sk", this.sk)
        this.builder.add("artist", this.artist, true)
        this.builder.add("track", this.track, true)
        this.builder.add("duration", this.duration)
        
        this.logger.info("Sending now playing notification..")
        
        this.postData(this.builder.WORKING_URL, handle)
    }
    
    afterPost(handle) {
        let data = Util.parseJson(this.xmlHttp.responseText)
        
        if (data.error) {
            this.logger.error("Encountered error when trying to update now playing: ")
            this.logger.error(data.message)
        } else {
            this.logger.info(`Now playing has been set to "${this.artist} - ${this.track}".`)
    
            if (Util.getValidRating(handle) === 5) {
                this.last_fm.post("track.love", null, handle)
            }
        }
    }
}
