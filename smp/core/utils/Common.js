"use strict"

/**
 * Serves as a container for util methods.
 *
 * Only here to make sure we're not clogging up the global namespace too much.
 *
 * This class relies a lot on the Variables.js file, so that needs to be included as well.
 *
 * These are mainly taken/derived from marc2003, so credits to him.
 */
class Util {
    
    static evalFormat(value, force = true) {
        return fb.TitleFormat(value)
                 .Eval(force)
    }
    
    static getCurrentDate() {
        // returns current time formatted like "2018-01-14 09:04:50"
        const currentDate = new Date()
        const currentTime = currentDate.getTime()
        const offset = currentDate.getTimezoneOffset() * 60 * 1000
        const tmp = new Date(currentTime - offset).toISOString()
        return tmp.substring(0, 10) + " " + tmp.substring(11, 19)
    }
    
    /**
     * Set's the rating of a track.
     * @param {Object} rating [The rating to be given. 0-5]
     * @param {Object} handle  [description] // meta_db
     */
    static setTrackRating(rating, handle) {
        if (rating > 5) {
            log.error("Rating is too high, it can only be between 0 and 5. Aborting.")
            return
        }
    
        const oldRating = this.getValidRating(handle)
    
        if (oldRating === rating) {
            return log.info(`This track already has a rating of ${rating}, aborting.`)
        }
    
        if (rating < 3 && oldRating >= 3 && oldRating > 0) {
            handle.SetLoved(0)
        } else if (rating >= 3) {
            handle.SetLoved(1)
        }
    
        fb.RunContextCommandWithMetadb("Playback Statistics/Rating/" + rating, track)
        handle.SetRating(rating)
        log.info(`Set rating of '${this.formatTitle("%title% by %artist%", handle)}' to ${rating}.`)
    
        handle.RefreshStats()
    }
    
    static getTrackInfo(handle, connector = "by", claw = true) {
        return `${this.formatTitle("%title%", handle)} ${connector} ${claw ?
                                                                      this.clawJapaneseNameFromEnclosure(
                                                                          this.formatTitle("%artist%", handle)) :
                                                                      this.formatTitle("%artist%", handle)}`
    }
    
    static getValidRating(handle) {
        // This can also be vanilla, because foo_playcount takes over the vanilla field.
        const foo_playcount = parseInt(this.formatTitle("$if2(%rating%,0)", handle))
        const foo_smp = parseInt(this.formatTitle("$if2(%smp_rating%,0)", handle))
        return foo_playcount > foo_smp ? foo_playcount : foo_smp
    }
    
    static getBiggestPlayCount(handle) {
        const _playCount = parseInt(Util.formatTitle("$if2(%play_count%,0)", handle))
        const _smpPlayCount = parseInt(Util.formatTitle("$if2(%smp_playcount%,0)", handle))
        return _playCount > _smpPlayCount ? _playCount : _smpPlayCount
    }
    
    static quote(value) {
        return "\"" + value + "\""
    }
    
    static parseJson(value) {
        try {
            return JSON.parse(value)
        } catch (e) {
            return []
        }
    }
    
    static parseJsonFile(file) {
        return this.parseJson(this.readFileToString(file))
    }
    
    static formatTitle(title, meta_db) {
        return !meta_db ? "" : fb.TitleFormat(title).EvalWithMetadb(meta_db)
        
    }
    
    static isTagged(value) {
        return value !== "" && value !== "?"
    }
    
    static runCommand(command, shouldWait) {
        try {
            WshShell.Run(command, 0, shouldWait)
        } catch (e) {
        }
    }
    
    static execute() {
        try {
            WshShell.Run(_.map(arguments, this.quote)
                          .join(" "))
            return true
        } catch (e) {
            return false
        }
    }
    
    // what
    static clawJapaneseNameFromEnclosure(input) {
        let fixed = input
        
        if (!fixed.contains("CV:") && fixed.contains("(") && fixed.endsWith(")")) {
            if (fixed.match(/[\u3000-\u303f\u3040-\u309f\u30a0-\u30ff\uff00-\uff9f\u4e00-\u9faf\u3400-\u4dbf]/)) {
                let encasedName = input.substring(0, fixed.length - 1)
                encasedName = encasedName.split("(")[1]
                fixed = encasedName
            }
        }
        
        return fixed
    }
}

/**
 * Utility methods for having some sort of "type safety" in JS.
 */
class TypeSafety {
    
    /**
     * Returns whether the type of the given `object` is that of a `String`.
     *
     * @param {*} object - The `object` to check against.
     * @return {Boolean} Whether the given `object` is a `String` or not.
     */
    static isString(object) {
        return typeof object === "string" || object instanceof String
    }
    
    /**
     * Returns whether the type of the given `object` is that of a `Boolean`.
     *
     * @param {*} object - The `object` to check against.
     * @return {Boolean} Whether the given `object` is a `Boolean` or not.
     */
    static isBoolean(object) {
        return typeof object === "boolean" || object instanceof Boolean
    }
    
    /**
     * Returns whether the type of the given `object` is that of a `Number`.
     *
     * @param {*} object - The `object` to check against.
     * @return {Boolean} Whether the given `object` is a `Number` or not.
     */
    static isNumber(object) {
        return typeof object === "number" || object instanceof Number
    }
}

/**
 * Utility methods for working with strings.
 */
class Strings {
    
    /**
     * Returns whether the given `string` is empty or `null`.
     *
     * @param {String} _string - The string to check against.
     * @return {Boolean} Whether the `string` is empty or `null`.
     */
    static isEmptyOrNull(_string) {
        const string = String(_string)
        
        return string == null || string.length <= 0
    }
}

/**
 * Utility methods for working with files.
 */
class Files {
    
    /**
     * Returns whether the `file` at the given `path` exists, regardless of the nature of it.
     *
     * @param {String} path - The path to the `file` to test against.
     * @returns {Boolean} Whether or not the `file` exists.
     */
    static exists(path) {
        return this.isFile(path) ? true : this.isDirectory(path)
    }
    
    /**
     * Returns whether or not the entity located at the given `path` is a file or not.
     *
     * @param {String} path - The path to the entity to check against.
     * @return {Boolean} Whether or not the entity located at the given `path` is a file or not.
     */
    static isFile(path) {
        return !Strings.isEmptyOrNull(path) && TypeSafety.isString(path) ? FileSystem.FileExists(path) : false
    }
    
    /**
     * Returns whether or not the entity located at the given `path` is a directory or not.
     *
     * @param {String} path - The path to the entity to check against.
     * @return {Boolean} Whether or not the entity located at the given `path` is a directory or not.
     */
    static isDirectory(path) {
        return !Strings.isEmptyOrNull(path) && TypeSafety.isString(path) ? FileSystem.FolderExists(path) : false
    }
    
    /**
     * Returns the parent of the entity located at the given `path`.
     *
     * **Note:** This function does *not* care whether there actually exists an entity at the given path.
     *
     * @param {String} path - The path to the entity to get the parent of.
     * @returns {String} The parent of the entity located at the given `path`.
     */
    static getParent(path) {
        return FileSystem.GetParentFolderName(path)
    }
    
    /**
     * Attempts to create any missing folders in the given path.
     *
     * **Note:** This assumes that the given `path` is supposed to lead to a directory, not a file.
     *
     * @param {String} path - The path to create from.
     * @return {String} The given `path` back, so that you can assign with this function.
     */
    static createDirectories(path) {
        const dirs = path.trim().split("\\")
        const driveName = this.getDriveName(path)
        //driveName = driveName.substring(0, driveName.length - 1)
        let traversedPath = driveName // We start the path with the drive name, because we won't be adding that via
        // the loop later.
        
        for (let dir of dirs) {
            if (dir === driveName) continue
            
            const name = this.getName(dir)
            
            // We don't want any empty dirs.
            if (Strings.isEmptyOrNull(name)) continue
            
            traversedPath += `\\${name}`
            
            // We don't want to try and create a directory on a path that already exists, because ActiveXObject will
            // throw an error then.
            if (this.isDirectory(traversedPath)) continue
            
            this.createDirectory(traversedPath)
            
            // If it is the last element in the path.
            if (name === this.getName(path)) {
                log.info(`Created all the directories in the "${path}" path.`)
            }
        }
        
        return path
    }
    
    static createDirectory(folder) {
        if (!this.isDirectory(folder)) FileSystem.CreateFolder(folder)
        return folder
    }
    
    /**
     * Returns the name of the drive the `path` uses.
     *
     * @param {String} path - The path to get the drive name from.
     * @return {String} The name of the drive that the given `path` uses.
     */
    static getDriveName(path) {
        return FileSystem.GetDriveName(path)
    }
    
    /**
     * Returns the name of the *last* object in the given `path`.
     *
     * **Note:** This function does *not* care about whether the last entity in the `path` actually exists.
     *
     * @param {String} path - The path to retrieve the `element` from.
     * @return {String} The name of the last `element` in the `path`.
     */
    static getName(path) {
        return FileSystem.GetFileName(path)
    }
    
    /**
     * Attempts to create a {@link GdiBitmap} from the given `path`, returning `null` if the given `path` is not an
     * actual file.
     *
     * @param {String} path - The path to the image file.
     * @returns {?GdiBitmap} A {@link GdiBitmap}, or `null` if the given `path` is invalid.
     */
    static readImage(path) {
        return this.isFile(path) ? gdi.Image(path) : null
    }
    
    /**
     * Attempts to read all the lines of the `file` located at the given `path` into a `String`, or it will return an
     * empty `String` if it was unsuccessful.
     *
     * @param {String} path - The path to the file to read from.
     * @returns {String} The lines of the file, or an empty `String` if unsuccessful.
     */
    static readLines(path) {
        return this.isFile(path) ? utils.ReadTextFile(path) : ""
    }
    
    static saveFile(file, value) {
        if (this.isDirectory(utils.FileTest(file, "split")[0]) && utils.WriteTextFile(file, value)) {
            return true
        }
        
        log.error(`Couldn't save to ${file}.`)
        
        return false
    }
    
    static getShortPath(file) {
        return FileSystem.GetFile(file).ShortPath
    }
    
    static deleteFile(file) {
        if (Files.isFile(file)) {
            try {
                FileSystem.DeleteFile(file)
            } catch (e) {
            }
        }
    }
}

/**
 * Utility methods for working with gui related tasks.
 */
class Gui {
    
    constructor() {
        this.tooltip = window.CreateTooltip("Segoe UI", _scale(12))
        this.tooltip.SetMaxWidth(1200)
    }
    
    static get DPI() { WshShell.RegRead("HKCU\\Control Panel\\Desktop\\WindowMetrics\\AppliedDPI") }
    
    static scaleSize(size) {
        return Math.round(size * this.DPI / 72)
    }
}

class SmpStats {
    
    static get PLAY_COUNT() { return parseInt(Util.evalFormat("$if2(%smp_playcount%,0)")) }
    
    static get RATING() { return parseInt(Util.evalFormat("$if2(%smp_rating%,0)")) }
    
    static get FIRST_PLAYED_DATE() { return Util.evalFormat("$if2(%smp_first_played%,N/A)") }
    
    static get LAST_PLAYED_DATE() { return Util.evalFormat("$if2(%smp_last_played%,N/A)") }
    
    static isEmpty() {
        return this.PLAY_COUNT === 0 && this.RATING === 0 && this.FIRST_PLAYED_DATE === "N/A" &&
               this.LAST_PLAYED_DATE === "N/A"
    }
    
    static runAll(handle, shouldRefresh = true, shouldPrint = false) {
        //log.info("Migrating all values..")
        this.migratePlayCount(handle, false)
        this.migrateRating(handle, true, false)
        this.updateFirstPlayed(handle, false)
        this.updateLastPlayed(handle, false)
        
        if (shouldRefresh) handle.RefreshStats()
        if (shouldPrint) log.info(`Track "${Util.getTrackInfo(handle)}" has been migrated to SMP stats.`)
    }
    
    static getPlayCount(handle) { return parseInt(Util.formatTitle("$if2(%smp_playcount%,0)", handle)) }
    
    static migratePlayCount(handle, shouldRefresh = true) {
        const _playCount = parseInt(fb.TitleFormat("$if2(%play_count%,0)").Eval())
        const _smpPlayCount = parseInt(fb.TitleFormat("$if2(%smp_playcount%,0)").Eval())
        
        if (_playCount > _smpPlayCount) {
            handle.SetPlaycount(_playCount)
        }
        
        if (shouldRefresh) handle.RefreshStats()
    }
    
    static increasePlayCount(handle, shouldRefresh = true) {
        const _playCount = parseInt(Util.formatTitle("$if2(%smp_playcount%,0)", handle))
        
        this.setPlayCount(_playCount + 1, handle, shouldRefresh)
    }
    
    static setPlayCount(newPlayCount, handle, shouldRefresh = true) {
        const _playCount = Util.getBiggestPlayCount(handle)
        
        if (_playCount > newPlayCount) {
            log.error(`newPlayCount(${newPlayCount}) must be larger than the already known play counts, aborting..`)
            return
        }
        
        handle.SetPlaycount(newPlayCount)
        
        if (shouldRefresh) handle.RefreshStats()
    }
    
    static migrateRating(handle, automaticLover = true, shouldRefresh = true) {
        const _rating = parseInt(Util.formatTitle("$if2(%rating%,0)", handle))
        const _smpRating = parseInt(Util.formatTitle("$if2(%smp_rating%,0)", handle))
    
        const test1 = _smpRating === 0
        const test2 = _rating > 0
        
        if (_smpRating === 0 && _rating > 0) {
            this.setRating(_rating, handle, automaticLover, shouldRefresh)
            // Reset the rating of the original track now that the value has been migrated to SMP.
            fb.RunContextCommandWithMetadb(`Playback Statistics/Rating/<not set>`, track)
        }
    }
    
    /**
     * Set's the rating of a track.
     * @param {Number} rating The rating to be given. 0..5
     * @param {Object} handle  The handle of the track. // meta_db
     * @param {Boolean} automaticLover Whether or not the system should automatically love/unlove the track based on the
     * rating given.
     * @param {Boolean} shouldRefresh Whether or not the {@link FbMetadbHandle.RefreshStats} should be called at the
     * end.
     */
    static setRating(rating, handle, automaticLover = true, shouldRefresh = true, includeFooPlayCount = false) {
        if (rating > 5) {
            log.error("Rating is too high, it can only be between 0 and 5. Aborting.")
            return
        }
    
        //const oldRating = Util.getValidRating(handle)
        const oldRating = this.RATING
        
        if (oldRating === rating) {
            return log.info(`This track already has a rating of ${rating}, aborting.`)
        }
        
        if (automaticLover) {
            if (rating < 3 && oldRating >= 3) {
                handle.SetLoved(0)
            } else if (rating >= 3) {
                handle.SetLoved(1)
            }
        }
        
        handle.SetRating(rating)
        if (includeFooPlayCount) fb.RunContextCommandWithMetadb(
            `Playback Statistics/Rating/${rating === 0 ? `<not set>` : rating}`, handle)
        log.info(`Set rating of "${Util.getTrackInfo(handle)}" to ${rating}.`)
        
        handle.RefreshStats()
    }
    
    static updateDates(handle) {
        this.updateFirstPlayed(handle, false)
        this.updateLastPlayed(handle, false)
        
        handle.RefreshStats()
    }
    
    static updateFirstPlayed(handle, shouldRefresh = true) {
        const _firstPlayed = Util.evalFormat("%first_played%")
        const _smpFirstPlayed = Util.evalFormat("%smp_first_played%")
    
        if ((_firstPlayed === "" || _firstPlayed === "N/A") && _smpFirstPlayed === "") {
            handle.SetFirstPlayed(Util.getCurrentDate())
            log.info(`First played date of "${Util.getTrackInfo(handle)}" has been set to "${Util.getCurrentDate()}".`)
        } else if (_firstPlayed !== "") {
            const _value = new Date(_firstPlayed).valueOf()
            
            if (_smpFirstPlayed === "") {
                handle.SetFirstPlayed(_firstPlayed)
                log.info(`First played date of "${Util.getTrackInfo(handle)}" has been set to "${_firstPlayed}".`)
            } else {
                const _smpValue = new Date(_smpFirstPlayed).valueOf()
                
                // Smaller value means that it was created at an earlier time.
                // This gets ran if the set smp date is *later* than the foo_playcount one.
                if (_smpValue > _value) {
                    handle.SetFirstPlayed(_firstPlayed)
                    log.info(`First played date of "${Util.getTrackInfo(handle)}" has been set to "${_firstPlayed}".`)
                } // else we just keep the value.
            }
        }
        
        if (shouldRefresh) handle.RefreshStats()
    }
    
    static updateLastPlayed(handle, shouldRefresh = true) {
        handle.SetLastPlayed(Util.getCurrentDate())
        if (shouldRefresh) handle.RefreshStats()
    }
}

class ParameterBuilder {
    
    constructor(url = "") {
        this.url = url
        this.parameters = ""
    }
    
    add(name, value, shouldEncode = false) {
        this.parameters += `&${name}=${shouldEncode ? encodeURIComponent(value) : value}`
    }
    
    clear() {
        this.parameters = ""
    }
    
    get WORKING_URL() { return `${this.url}${this.parameters.substring(1)}` }
    
    //toString() { this.WORKING_URL }
}