// ==PREPROCESSOR==
// @name "Last.fm Playcount"
// @author "phts"
// @import "%fb2k_path%skins\Kanon\jscript\Playcount Sync Legacy\js\json2.js"
// @import "%fb2k_path%skins\Kanon\jscript\Playcount Sync Legacy\js\lodash.min.js"
// @import "%fb2k_path%skins\Kanon\jscript\Playcount Sync Legacy\js\helpers.js"
// @import "%fb2k_path%skins\Kanon\jscript\Playcount Sync Legacy\js\panel.js"
// @import "%fb2k_path%skins\Kanon\jscript\Playcount Sync Legacy\js\lastfm.js"
// @import "%fb2k_path%skins\Kanon\jscript\Playcount Sync Legacy\js\scrobbler.js"
// @import "%fb2k_path%skins\Kanon\jscript\Essential\VariableHelpers.js"
// ==/PREPROCESSOR==

var panel = new _.panel("Lastfm Playcount");
var buttons = new _.buttons();
var lastfm = new _.lastfm();

var scrobbler = new _.scrobbler(0, 0, 32);
scrobbler.update_button();

function on_notify_data(name, data) {
    lastfm.notify_data(name, data);
    scrobbler.notify_data(name, data);
}

function on_size() {
    panel.size();
    InitInjection();
}

function on_paint(gr) {
    panel.paint(gr);
    buttons.paint(gr);
}

function on_playback_new_track() {
    scrobbler.playback_new_track();
}

function on_playback_time() {
    scrobbler.playback_time();
    scrobbler.playcount_sync();
}

function on_mouse_lbtn_up(x, y) {
    buttons.buttons.scrobbler.lbtn_up(x, y);
}
